-- Lua script of map overworld.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map becomes is loaded.
function map:on_started()
  local hero = map:get_hero()

  local gravestone = map:get_entity("gravestone")
  function gravestone:on_interaction()
    game:start_dialog("beach.gravestone", function(status)
      if not game:has_item("tears") then
        -- Get male tears
        hero:start_treasure("tears")
      end
    end)
  end

  local trash_seagull_01 = map:get_entity("trash_seagull_01")
  function trash_seagull_01:on_interaction()
    if self:get_sprite():get_animation() == "normal" then
      game:start_dialog("beach.seagulls.figured_out")
    else
      game:start_dialog("beach.seagulls.trash")
    end
  end

  local trash_seagull_02 = map:get_entity("trash_seagull_02")
  function trash_seagull_02:on_interaction()
    if self:get_sprite():get_animation() == "normal" then
      game:start_dialog("beach.seagulls.uncomfortable")
    else
      game:start_dialog("beach.seagulls.sucks")
    end
  end

  local trash_seagull_03 = map:get_entity("trash_seagull_03")
  function trash_seagull_03:on_interaction()
    if self:get_sprite():get_animation() == "normal" then
      game:start_dialog("beach.seagulls.transgressions")
    else
      game:start_dialog("beach.seagulls.compatriots")
    end
  end

  local trash_seagull_04 = map:get_entity("trash_seagull_04")
  function trash_seagull_04:on_interaction()
    if self:get_sprite():get_animation() == "normal" then
      game:start_dialog("beach.seagulls.poseidons_daughter")
    else
      game:start_dialog("beach.seagulls.great_again")
    end
  end

  do
    local sensor = map:get_entity("bridge_1")
    function sensor:on_activated()
      local x, y, z = hero:get_position()
      hero:set_position(x, y, 1)
      log("Hero moved to L1: ", hero:get_position())
    end
  end
  do
    local sensor = map:get_entity("bridge_2")
    function sensor:on_activated()
      local x, y, z = hero:get_position()
      hero:set_position(x, y, 0)
      log("Hero moved to L0: ", hero:get_position())
    end
  end

  local big_rafflesia = map:get_entity("big_rafflesia")
  big_rafflesia:set_traversable_by("hero", false)

  local ocean_1 = map:get_entity("ocean_1")
  function ocean_1:on_activated()
    game:start_dialog("beach.ocean.sadness")
  end

  local ocean_2 = map:get_entity("ocean_2")
  function ocean_2:on_activated()
    game:start_dialog("beach.ocean.grave")
  end

  local ocean_3 = map:get_entity("ocean_3")
  function ocean_3:on_activated()
    game:start_dialog("beach.ocean.oil_spill")
  end

end
