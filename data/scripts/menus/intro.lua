-- Intro sequence that plays at the start of the game

-- Load scenes
local scenes = {
  require("scripts/menus/intro_scenes/letter"),
  require("scripts/menus/intro_scenes/hmm"),
  require("scripts/menus/intro_scenes/ok"),
  require("scripts/menus/intro_scenes/capn"),
  require("scripts/menus/intro_scenes/shipwreck"),
}


local intro = {}

-- Called when the intro menu is started with sol.menu.start()
function intro:on_started()
  log("Intro started")
  sol.audio.play_music("cosmicgem829/intro")

  -- Loop through the scenes
  for i, scene in ipairs(scenes) do
    -- When each scene ends, play the next one
    function scene:on_finished()
      local next_scene = scenes[i+1]
      if next_scene then
        sol.menu.start(intro, next_scene)
      else
        -- Final scene, stop the intro menu
        sol.menu.stop(intro)
      end
    end
  end

  -- Start the first scene
  sol.menu.start(intro, scenes[1])
end


-- Let the intro be skipped
function intro:on_command_pressed(command)
  if command == "action" or command == "pause" or command == "item_1" then
    sol.menu.stop(intro)
  end
  return true
end


return intro
