-- Initialize hero behavior specific to this quest.

require("scripts/multi_events")
require("scripts/utils")
local all = require("scripts/meta/all")

local hero_meta = sol.main.get_metatable("hero")

local function initialize_hero_features(game)

  local hero = game:get_hero()
  -- In this game you can never die
  hero:set_invincible()
  hero:set_walking_speed(80)

  function silence_music()
    -- silence music for brandish SFX
    local volume = sol.audio.get_music_volume()
    sol.audio.set_music_volume(0)
    log("Music volume: 0")
    local timer = sol.timer.start(2600, function()
      log("Music volume: 100")
      sol.audio.set_music_volume(volume)
    end)
    timer:set_suspended_with_map(false)
  end

  -- The hero can only swing the sword, nothing else
  function hero:on_state_changed(state)
    if state == "sword loading"
    or state == "sword tapping"
    or state == "sword spin attack" then
      hero:freeze()
      hero:unfreeze()
    end
  end

  -- The hero will brandish an item without obtaining it.
  -- Reimplementation of hero:start_treasure() except you don't get the item.
  function hero:brandish(item_key, variant)
    self:freeze()
    silence_music()
    sol.audio.play_sound("treasure")

    -- Set some variables
    if not variant then variant = 1 end
    local x, y, z = self:get_position()
    local item = hero:get_game():get_item(item_key)
    assert(item)

    -- Set the hero's sprite
    local hero_sprite = hero:get_sprite()
    hero_sprite:set_animation("brandish")

    -- Create a map entity for the item to show during brandishing
    local map = item:get_map()
    local item_entity = map:create_custom_entity({
      sprite = "entities/items",
      direction = variant - 1,
      x=0, y=0, layer=0, width=16, height=16
    })
    local item_sprite = item_entity:get_sprite()
    item_sprite:set_animation(item_key)
    local ox, oy = item_sprite:get_origin()
    item_entity:set_position(x, y-28+oy, z+1)

    -- Start item dialog
    local game = item:get_game()
    game:start_dialog("_treasure." .. item_key .. "." .. variant, function()
      -- End
      self:unfreeze()
      hero_sprite:set_animation("stopped")
      item_entity:remove()
    end)
  end

  -- Make the hero blink her eyes every 6 seconds
  function hero:on_created()
    -- Every 6 seconds...
    sol.timer.start(self, 6000, function()

      local sprite = self:get_sprite()
      local is_stopped = sprite:get_animation() == "stopped"  -- Only "stopped" has a blinking animation

      if is_stopped then
        sprite:set_animation("blink", "stopped")
      end

      return true  -- loop forever
    end)
  end

  function hero:on_state_changed(new_state_name)
    if new_state_name == "treasure" then
      local item_star = self:create_sprite("menus/item_star", "item_star")
      item_star:set_ignore_suspend()
      self:bring_sprite_to_back(item_star)

      -- HACK: Layer the tunic sprite on again because
      -- setting the sprite order doesn't affect the hero
      -- https://gitlab.com/solarus-games/solarus/issues/1348#note_142442035
      local tunic_sprite = hero:get_sprite("tunic")
      local tunic_top = hero:create_sprite(tunic_sprite:get_animation_set(), "tunic_top")
      tunic_top:set_direction(tunic_sprite:get_direction())
      tunic_top:set_animation("brandish")
      -- END HACK

      force_animation(item_star, true)
      silence_music()
    end
  end

  function hero:on_state_changing(state_name, next_state_name)
    local item_star = self:get_sprite("item_star")
    local tunic_top = self:get_sprite("tunic_top") -- HACK
    if state_name == "treasure" then
      if item_star then self:remove_sprite(item_star) end
      if tunic_top then self:remove_sprite(tunic_top) end -- HACK
    end
  end

  -- Rachel has a thought bubble animation
  function hero:think()
    all.think(self)
    force_animation(self:get_sprite("thought_bubble"))
  end

end

-- Set up Eldran hero sprite on any game that starts.
local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", initialize_hero_features)
return true
