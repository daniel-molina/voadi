local all = require("scripts/meta/all")
local picker = require("scripts/menus/picker")
local npc_meta = sol.main.get_metatable("npc")

function npc_meta:on_created()
  local game = self:get_game()
  local name = self:get_name()
  local cleaned = false

  if name then
    cleaned = game:get_value(name.."__cleaned")
  end

  if cleaned then
    self:get_sprite():set_animation("normal")
  end
end

-- Start a picker menu for the NPC
-- https://gitlab.com/voadi/voadi/wikis/docs/lua-api#npcprompt_itemcallback
function npc_meta:prompt_item(callback, filter)
  if not filter then -- default filter shows only key items
    filter = function(item) return item:is_key_item() end
  end
  picker._npc = self
  picker._filter = filter
  sol.menu.start(self:get_map(), picker)
  function picker:on_finished()
    callback(self:get_selection())
    picker._npc = nil
    picker._filter = nil
    self.on_finished = nil -- destroy this function between calls
  end
end

-- Enable thought bubble
npc_meta.think = all.think

-- Let NPC sprites be animated even on suspend
function npc_meta:on_suspended()
  local sprite = self:get_sprite()
  if sprite then sprite:set_ignore_suspend() end
end

-- Like `game:start_dialog()` except it uses a talking animation if that exists
function npc_meta:start_dialog(dialog_id, callback)
  local sprite = self:get_sprite()
  sprite:set_ignore_suspend()
  if sprite:has_animation("talking") then
    sprite:set_animation("talking")
  end
  local new_callback = function() sprite:set_animation("stopped") end
  if callback then
    function new_callback(...)
      sprite:set_animation("stopped")
      callback(...)
    end
  end
  self:get_game():start_dialog(dialog_id, nil, new_callback)
end

-- Set default on_vacuum_changed event for NPCs.
-- Namely, if their animation is called "trash", set it to "normal" after power level >= 3.
function npc_meta:on_vacuum_changed(power)
  local game = self:get_game()
  local sprite = self:get_sprite()
  local animation = sprite:get_animation()

  -- Set to normal
  if power >= 3
     and animation == "trash"
     and sprite:has_animation("normal")
  then
    sprite:set_animation("normal")
    game:set_value(self:get_name().."__cleaned", true)
    log(string.format("NPC (%d, %d) set to normal by vacuum.", self:get_position()))
  end
end
