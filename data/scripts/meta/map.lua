local tile_puzzle = require("scripts/tile_puzzle")
local map_meta = sol.main.get_metatable("map")

-- Return a list of puzzles on the map
function map_meta:get_puzzles()
  return self.tile_puzzles -- set by: entities/maze_tile.lua
end

-- Get a puzzle if it exists, or create one with the given name if not
function map_meta:get_puzzle(name)
  self.tile_puzzles[name] = self.tile_puzzles[name] or tile_puzzle:new()
  return self.tile_puzzles[name]
end

-- Destroy all puzzles associated with this map
function map_meta:clear_puzzles()
  self.tile_puzzles = {}
  log("Puzzles destroyed from map.")
end

-- Destroy puzzles by default when map is left
function map_meta:on_finished()
  self:clear_puzzles()
end

-- Implement camera pan effect
function map_meta:focus_on(target_entity, callback)
  local hero = self:get_hero()
  hero:freeze()
  local camera = self:get_camera()
  local m = sol.movement.create("target")
  m:set_target(camera:get_position_to_track(target_entity))
  m:set_speed(160)
  m:set_ignore_obstacles(true)
  m:start(camera, function()
    callback()
    sol.timer.start(camera:get_map(), 500, function()
      m:set_target(camera:get_position_to_track(hero))
      m:set_speed(160)
      m:start(camera, function() hero:unfreeze() camera:start_tracking(hero) end)
    end)
  end)
end

-- returns true if the map currently has trash entities on it
function map_meta:has_trash()
  -- Name of the savegame variable for this map and model
  local valname = string.format("entitystate__%s__%s", path_encode(self:get_id()), "trash")
  local trash_state_str = self:get_game():get_value(valname)

  for i = 1, #trash_state_str do
    local c = trash_state_str:sub(i, i)
    if c == "1" then
      return true -- 1 represents uncleaned trash. a single 1 means we have trash.
    end
  end
  return false
end

-- Called whenever a trash entity (from this map) is destroyed
function map_meta:on_trash_removed(trash_entity)
  if self:has_trash() == false then
    -- Trash is cleaned, roll the credits
    -- TODO: Do something.
  end
end
