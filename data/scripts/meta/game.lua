require("scripts/coroutine_helper")

local end_credits = require("scripts/menus/end_credits")
local confetti = require("scripts/menus/confetti")
local save_feedback = require("scripts/menus/save_feedback")

local game_meta = sol.main.get_metatable("game")

-- Adds game:roll_credits() which sequences out the last part of the game.
function game_meta:roll_credits()
  sol.menu.start(self, end_credits)
end

-- Make confetti fall across the screen
function game_meta:start_confetti()
  sol.menu.start(self, confetti)
end

-- Event that will be called when the game is saved
function game_meta:on_saved()
  log("Game saved")
  sol.main.start_coroutine(function()
    run_on_main(function() sol.menu.start(self, save_feedback) end)
    local m1 = sol.movement.create("straight")
    m1:set_angle(math.pi/2)
    m1:set_max_distance(16)
    m1:set_speed(500)
    movement(m1, save_feedback.feedback_bg)
    wait(3000)
    local m2 = sol.movement.create("straight")
    m2:set_angle(3*math.pi/2)
    m2:set_max_distance(16)
    m2:set_speed(32)
    movement(m2, save_feedback.feedback_bg)
    run_on_main(function() sol.menu.stop(save_feedback) end)
  end)
end

-- Override game:save()
game_meta._save = game_meta.save
function game_meta:save()
  self:_save()
  self:on_saved()
end

-- Check whether a picker is already active
-- https://gitlab.com/voadi/voadi/wikis/docs/lua-api#gameis_picker_enabled
function game_meta:is_picker_enabled()
  return self._picker_enabled
end

-- Initialize game with _picker_enabled attribute
game_meta:register_event("on_started", function(self)
  self._picker_enabled = false
end)
